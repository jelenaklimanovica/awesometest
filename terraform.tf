terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    bucket = "team-1-project-bucket"
    key = "terraform.tfstate"
    encrypt = true
  }
}